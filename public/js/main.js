/*
=============
PopUp
=============
 */
const popup = document.querySelector(".popup");
const closePopup = document.querySelector(".popup__close");


if (popup) {
  closePopup.addEventListener("click", () => {
    popup.classList.add("hide__popup");
  });

  window.addEventListener("load", () => {
    setTimeout(() => {
      popup.classList.remove("hide__popup");
    }, 500);
  });
}

/*
=============
Scrolling
=============
*/

const scrollBtn = document.querySelector('.scrolling');

scrollBtn.addEventListener('click', () => window.scrollTo({
    top: 0,
    behavior: 'smooth',
}));

window.onscroll = function () { scrollFunction() };

function scrollFunction() {
    if (document.body.scrollTop > 800 || document.documentElement.scrollTop > 800) {
        scrollBtn.style.display = "block";
    } else {
        scrollBtn.style.display = "none";
    }
}



