
const productFromDom = document.querySelector('.all_products');
const itemTotalFromDom = document.querySelector('.item_total');
const cartTotalFromDom = document.querySelector('.cart_total');
const cartContentFromDom = document.querySelector('.cart_content');
const clearButton = document.querySelector('.clear_button');
const empty = document.querySelector('.empty');
const productsRightInBox = document.querySelector('.products-right-box');


let cart = [];
let buttonFromDom = [];


class UI {
    static displayProducts(productsObj) {
        let results = ''
        productsObj.forEach(({ name, image, price, _id }) => {
            results += `
            <div class="col-4 mb-4">
                    <div class="card card_style">
                        <img src="${image}" class="card-img-top">
                        <div class="card-body">
                            <h5 class="card-title">${name}</h5>
                            <p class="card-text">This is a longer card with supporting text below as a natural lead-in to
                                additional content. This content is a little bit longer.</p>
                            <p class="card-text font-weight-bold">${price}$</p>
                            <button class="btn btn-outline-danger rounded-0 addToCart" data-id="${_id}" type="submit">Add to cart</button>
                            <button class="btn btn-outline-danger rounded-0 viewButton" data-id="${_id}" type="submit">View</button>                            
                        </div>
                    </div>
                </div>
            `
        });
        productFromDom.innerHTML = results;
    }

    static getButtons() {
        const buttons = [...document.querySelectorAll('.addToCart')]
        buttonFromDom = buttons;

        buttons.forEach(button => {
            const id = button.dataset.id
            const inMyCart = cart.find(item => item.id === parseInt(id, 10));
            if (inMyCart) {
                button.innerText = 'Added';
                button.disabled = true;
            }

            button.addEventListener('click', (e) => {
                e.preventDefault();
                e.target.innerHTML = 'Added';
                e.target.disabled = true;

                // Step-1: Get Products from LocalStorage
                const cartItem = { ...Storage.getProducts(id), amount: 1 };
                // Step-2: Add Product to cart
                cart = [...cart, cartItem]

                // Step-3: Save the products that are in cart to Store
                Storage.saveCart(cart)

                // Step-4: setItemValues
                this.setItemValues(cart);

                //Step-5: Dislaying the Items in the cart
                this.addToCart(cartItem)

            })
        })
    }

    static setItemValues(cart) {
        let total = 0;
        let itemTotal = 0;

        cart.map(item => {
            total += item.price * item.amount;
            itemTotal += item.amount
        })

        itemTotalFromDom.innerText = itemTotal;
        cartTotalFromDom.innerText = total;
    }

    static addToCart(cartItem) {

        let div = document.createElement('div');
        div.classList.add('col-12')
        div.innerHTML = `
                <div class="card mb-4 card_style">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-2">
                                <img src="${cartItem.image}" alt="" class="w-75">
                            </div>
                            <div class="col-5 ml-n4">
                                <h4 class="card-title">${cartItem.name}</h4>
                                <p>This is a longer card with supporting text below as a natural lead-in to
                                additional content. This content is a little bit longer.</p>
                                <p class="card-text price font-weight-bold">$${cartItem.price}</p>
                            
                            </div>
                            <div class="col-2 ">
                                <div class="row">
                                    <div class="col-12">
                                        <span data-id=${cartItem._id} class="increment btn btn-outline-danger">
                                                <i class="fas fa-chevron-up"></i>
                                        </span>
                                    </div>
                                    <div class="col-12 mt-3 ml-3">
                                        <p>${cartItem.amount}</p>
                                    </div>
                                    <div class="col-12">
                                        <span data-id=${cartItem._id} class="decrement btn btn-outline-warning">
                                            <i class="fas fa-chevron-down"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-2 align-self-center text-right">
                                <span href="#" class="btn btn-outline-danger remove_cart_item" data-id=${cartItem._id}><i class="fad fa-trash-alt"></i> </span>
                            </div>
                        </div>
                    </div>
                </div>
        
        `

        cartContentFromDom.appendChild(div);

        // Empty Content
        if (cartContentFromDom.children.length > 0) {
            if (cartContentFromDom.children.length === 1) {
                empty.classList.add('remove_empty_card')
            }
            // Show Total amount 
            document.querySelector('.total-amount').classList.remove('total-amount-hide');
        }


    }

    static settings() {
        cart = Storage.getCart();
        this.setItemValues(cart);
        this.populate(cart);
    }

    static populate(cart) {
        cart.forEach(item => this.addToCart(item))
    }

    static logicInCart() {

        // clear Cart
        clearButton.addEventListener('click', () => {
            this.clearProductsInCart();
        })
        cartContentFromDom.addEventListener('click', (e) => {
            const target = e.target.closest('span');
            const allDecrementBtns = [...document.querySelectorAll('.decrement')]


            if (!target) { return };
            const targetElement = target.classList.contains('remove_cart_item');

            // Remove Single Element from CartDOM
            if (targetElement) {
                const id = target.dataset.id
                this.removeItem(id);
                cartContentFromDom.removeChild(target.parentElement.parentElement.parentElement.parentElement.parentElement);
                if (cartContentFromDom.children.length === 0) {
                    empty.classList.remove('remove_empty_card')

                    // Hide Total amount 
                    document.querySelector('.total-amount').classList.add('total-amount-hide');
                }
            }
            else if (target.classList.contains('increment')) {
                const id = target.dataset.id;
                let productFromCart = cart.find(item => item._id == id)
                productFromCart.amount++;
                if (productFromCart.amount > 1) {
                    const decrementBtn = allDecrementBtns.find(item => item.dataset.id == id);

                    if (decrementBtn.classList.contains('hide_decrement')) {
                        decrementBtn.classList.remove('hide_decrement');
                    }

                }
                Storage.saveCart(cart);
                this.setItemValues(cart);
                target.parentElement.nextElementSibling.childNodes[1].innerText = productFromCart.amount
            }
            else if (target.classList.contains('decrement')) {
                const id = target.dataset.id;
                let productFromCart = cart.find(item => item._id == id)
                productFromCart.amount--;

                if (productFromCart.amount <= 1) {
                    const decrementBtn = allDecrementBtns.find(item => item.dataset.id == id);
                    decrementBtn.classList.add('hide_decrement');
                }

                if (productFromCart.amount > 0) {
                    Storage.saveCart(cart);
                    this.setItemValues(cart);
                    target.parentElement.previousElementSibling.childNodes[1].innerText = productFromCart.amount
                }
            }
            else {
                this.removeItem(id);
                cartContentFromDom.removeChild(target.parentElement.parentElement);
            }
        })


    }

    static clearProductsInCart() {

        const cartItems = cart.map(item => item._id);
        cartItems.forEach(id => this.removeItem(id));

        while (cartContentFromDom.children.length > 0) {
            cartContentFromDom.removeChild(cartContentFromDom.children[0]);
        }

        // Empty Content
        if (cartContentFromDom.children.length === 0) {
            empty.classList.remove('remove_empty_card')

            // Hide Total amount 
            document.querySelector('.total-amount').classList.add('total-amount-hide');
        }

    }

    static removeItem(id) {
        cart = cart.filter(item => item._id !== id);
        this.setItemValues(cart);
        Storage.saveCart(cart);

        let button = this.singleButton(id);
        button.disabled = false;
        button.innerText = 'Add to Cart';
    }
    static singleButton(id) {
        return buttonFromDom.find(button => button.dataset.id === id)

    }


}

class Storage {
    static saveProducts(productsObj) {
        localStorage.setItem('products', JSON.stringify(productsObj));
    }

    static saveCart(cart) {
        localStorage.setItem('carts', JSON.stringify(cart));
    }

    static getProducts(id) {
        const products = JSON.parse(localStorage.getItem('products'));
        return products.find(item => item._id === id)
    }

    static getCart() {
        return localStorage.getItem('carts') ? JSON.parse(localStorage.getItem('carts')) : [];
    }
}

class Products {
        
    async getProducts() {
        try {
            const results = await fetch(`https://e-business-development.herokuapp.com/api/products`);
            const data = await results.json();
            const products = data
            return products
        } catch (err) {
            console.log(err)
        }

    }




}
document.addEventListener('DOMContentLoaded', async () => {
    const ui = new UI();
    const products = new Products();

    UI.settings();
    UI.logicInCart();
    const productsObj = await products.getProducts();
    UI.displayProducts(productsObj);
    UI.getButtons();
    Storage.saveProducts(productsObj);


})
