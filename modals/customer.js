const mongoose = require('mongoose');
const Joi = require('joi');

const customerSchema = new mongoose.Schema({
    name : {
        type: String,
        required: true,
        minlength: 5,
        maxlength:50
    },
    isVip : {
        type: Boolean,
        default: false 
    },
    phone : {
        type: String,
        required: true,
        minlength: 5,
        maxlength:50
    },
    bonusPoints: {
        type: Number,
        default: 0
    }
})

const Customer = mongoose.model('Customer', customerSchema);

function validateCustomer(customers) {
    const schema = Joi.object({
        name: Joi.string().required().min(5).max(50),
        isVip: Joi.boolean().required(),
        phone: Joi.string().min(5).max(50).required(),
        bonusPoints: Joi.number().min(0)
    }) ;

    return schema.validate(customers);
}

exports.Customer = Customer;
exports.validate = validateCustomer;