const mongoose = require('mongoose');
const Joi = require('joi');
const { categorySchema } = require('./category')

// Schema for course
const Course = mongoose.model('Course', new mongoose.Schema({
    title: {
        type: String,
        required: true,
        minlength: 5,
        trim: true,
        maxlength: 50
    },
    categorie: {
        type: categorySchema
    },
    trainer: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 50
    },
    status: {
        type: String,
        enum: ['active', 'inactive'],
        required: true,
        minlength: 5
    },
    fee: Number
}));

function validateCourses(courses) {
    const schema = Joi.object({
        title: Joi.string().required().min(5).max(50),
        trainer: Joi.string().required(),
        status: Joi.string().required().min(5).max(50)
    }) ;

    return schema.validate(courses);
}


exports.Course = Course;
exports.validateCourses = validateCourses;
