const mongoose = require('mongoose');
const Joi = require('joi');
const jwt = require('jsonwebtoken');
const config = require('config'); 

const userSchema = new mongoose.Schema({
    name : {
        type: String,
        required: true,
        minlength: 5,
        maxlength:50
    },
    email : {
        type: String,
        required: true,
        minlength: 5,
        maxlength:255,
        unique: true
    },
    password : {
        type: String,
        required: true,
        minlength: 5,
        maxlength:1024
    },
    isAdmin: Boolean
})

userSchema.methods.generateAuthToken = function () {
  const token =  jwt.sign({ _id: this._id, isAdmin: this.isAdmin }, config.get('jwtPrivateKey'))
  return token
}

const User = mongoose.model('User', userSchema);

function validateUser(users) {
    const schema = Joi.object({
        name: Joi.string().min(1).max(50).required(),
        email: Joi.string().min(2).max(50).required().email(),
        password: Joi.string().min(5),
        isAdmin: Joi.boolean().required()
    }) ;

    return schema.validate(users);
}

exports.User = User;
exports.validateUser = validateUser;