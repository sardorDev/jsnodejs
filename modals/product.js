const mongoose = require('mongoose');
const Joi = require('joi');

const productSchema = new mongoose.Schema({
    name : {
        type: String,
        required: true,
        minlength: 3,
        maxlength:50
    },
    image : {
        type: String,
        required: true,
        minlength: 1,
        maxlength:50
    },
    price : {
        type: Number,
        required: true,
        minlength: 1,
        maxlength: 100000
    },
    sale: {
        type: Number,
        default: 0
    }
})

const Product = mongoose.model('Product', productSchema);

function validateProduct(products) {
    const schema = Joi.object({
        name: Joi.string().required().min(5).max(50),
        image: Joi.string().min(1).max(50).required(),
        price: Joi.number().min(1).max(100000).required(),
        sale: Joi.number().min(0)
    }) ;

    return schema.validate(products);
}

exports.Product = Product;
exports.validate = validateProduct;