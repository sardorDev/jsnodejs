const express = require('express');
const app = express();
const cors = require('cors');
const winston = require('winston');
require('./startup/logging')();
require('./startup/routes')(app);
require('./startup/db')();
require('./startup/config')();
require('./startup/prod')(app);

app.use(cors());

const port = process.env.PORT || 5005;
const server = app.listen(port, () => {
    winston.info(`${port} listen...`);
});

module.exports = server;