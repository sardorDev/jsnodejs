const express = require('express');
const router = express.Router();
const { User } = require('../modals/user');
const bcrypt = require('bcrypt');
const Joi = require('joi');

const _ = require('lodash')

router.get('/', async (req, res) => {
    const users = await User.find().sort('name');
    res.send(users);
})

router.post('/', async (req, res) => {
    const { error } = validate(req.body);
    if (error) {
        return res.status(400).send(error.details[0].message);
    }

    let user = await User.findOne({ email: req.body.email });
    if (!user) {
        return res.status(400).send('Email or password is incorrect');
    }

    const isValidPassword = await bcrypt.compare(req.body.password, user.password);

    if (!isValidPassword) {
        return res.status(400).send('Email or password is incorrect');
    }

    const token = user.generateAuthToken();
    res.header('x-auth-token', token).send(true)
})

function validate(req) {
    const schema = Joi.object({
        email: Joi.string().min(2).max(50).required().email(),
        password: Joi.string().min(5)
    });

    return schema.validate(req);
}

module.exports = router