const express = require('express');
const router = express.Router();
const { Course, validateCourses } = require('../modals/course');
const { Category } = require('../modals/category');
const mongoose = require('mongoose');

router.get('/', async (req, res) =>{
    const courses = await Course.find().sort('title'); 
    res.send(courses);
})

router.post('/', async (req, res)=>{
    const { error } = validateCourses(req.body); 
    if(error) { 
        return res.status(400).send(error.details[0].message);
    }

    const categorie  = await Category.findById(req.body.categorieId)
    if(!categorie ) {
        return res.status(400).send('Berilgan Id ga teng bölgan toifa topilmadi');
    }

    let course = new Course({
        title: req.body.title,
        categorie: {
            _id: categorie._id,
            name: categorie.name
        },
        trainer: req.body.trainer,
        status: req.body.status

    });
    course = await course.save();

    res.send(course);
})

module.exports = router





