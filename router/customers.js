const express = require('express');
const router = express.Router();
const { Customer,  validate } = require('../modals/customer');

router.get('/', async (req, res) =>{
    const customers = await Customer.find().sort('name'); 
    res.send(customers);
})

router.post('/', async (req, res)=>{ 
    const { error } = validate(req.body); 
    if(error) { 
        return res.status(400).send(error.details[0].message);
    }

    let customers = new Customer({
        name: req.body.name,
        isVip: req.body.isVip,
        phone: req.body.phone,
        bonusPoints: req.body.bonusPoints
    });

    customers = await customers.save();

    res.status(201).send(customers);
})

router.put('/:id', async (req, res)=>{  
    const { error } = validate(req.body); 
    if(error) { 
        return res.status(400).send(error.details[0].message);
    }

    let customers = new Customer({
        name: req.body.name
    });
    
    customers = await Customer.findByIdAndUpdate(req.params.id, { name: req.body.name }, {
        new: true 
    });        
    if(!customers) {
        return res.status(404).send('No book was found for the given id')
    }

    res.send(customers); 
})

router.delete('/:id', async (req, res) =>{
    let customers = await Customer.findByIdAndRemove(req.params.id);
    if(!customers){
        return res.status(404).send('No book was found for the given id');
    }

    res.send(customers);
})


module.exports = router