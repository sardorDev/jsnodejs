const express = require('express');
const router = express.Router();
const { Product,  validate } = require('../modals/product');


router.get('/', async (req, res) =>{
    const products = await Product.find().sort('name'); 
    res.send(products);
})

router.get('/price', async (req, res) =>{
    
    const productsPrice = await Product.find({ price: { $gt: 100, $lt: 180  }})
        .sort({name: 1 })
        res.send(productsPrice)
})

router.post('/', async (req, res)=>{ 
    const { error } = validate(req.body); 
    if(error) { 
        return res.status(400).send(error.details[0].message);
    }

    let products = new Product({
        name: req.body.name,
        image: req.body.image,
        price: req.body.price,
        sale: req.body.sale
    });

    products = await products.save();

    res.status(201).send(products);
})

/*

router.put('/:id', async (req, res)=>{  
    const { error } = validate(req.body); 
    if(error) { 
        return res.status(400).send(error.details[0].message);
    }

    let customers = new Customer({
        name: req.body.name
    });
    
    customers = await Customer.findByIdAndUpdate(req.params.id, { name: req.body.name }, {
        new: true 
    });        
    if(!customers) {
        return res.status(404).send('No book was found for the given id')
    }

    res.send(customers); 
})

router.delete('/:id', async (req, res) =>{
    let customers = await Customer.findByIdAndRemove(req.params.id);
    if(!customers){
        return res.status(404).send('No book was found for the given id');
    }

    res.send(customers);
})
*/

module.exports = router