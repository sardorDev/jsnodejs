const express = require('express');
const expressLayouts = require('express-ejs-layouts');
const errorMiddleware = require('../middleware/error');
const category = require('../router/categories');
const customer = require('../router/customers');
const course = require('../router/courses');
const user = require('../router/users');
const product = require('../router/products')
const auth = require('../router/auth');
const home = require('../router/home');
const entrollmentsRoute = require('../router/enrollments');

module.exports = function (app) {

//EJS
app.use(expressLayouts);
app.set('view engine', 'ejs');

// Static Files
app.use(express.static('public'));
app.use('/css', express.static(__dirname + 'public/css'));
app.use('/img', express.static(__dirname + 'public/img'));
app.use('/js', express.static(__dirname + 'public/js'));

//Routes
app.use(express.json());
app.use('/api/categories', category);
app.use('/api/customers', customer)
app.use('/api/courses', course);
app.use('/api/users', user);
app.use('/api/enrollments', entrollmentsRoute);
app.use('/api/products', product)
app.use('/api/auth', auth); 
app.use('/home', home);
app.use(errorMiddleware)

}